#%Module1.0

module-whatis	        "Anisotropic Correction of Beam-induced Motion for Improved Single-particle Electron Cryo-microscopy"
module-url		"http://msg.ucsf.edu/em/software/motioncor2.html"
module-license	        "This software may only be downloaded and used for free by academic and/or non-profit users"
module-maintainer	"Spencer Bliven <spencer.bliven@psi.ch>"

module-help	"
Correction of electron beam-induced sample motion is one of the major factors
contributing to the recent resolution breakthroughs in cryo-electron
microscopy. Based on observations that the electron beam induces doming of the
thin vitreous ice layer, we developed an algorithm to correct anisotropic image
motion at the single pixel level across the whole frame, suitable for both
single particle and tomographic images. Iterative, patch-based motion detection
is combined with spatial and temporal constraints and dose weighting. The
multi-GPU accelerated program, MotionCor2, is sufficiently fast to keep up with
automated data collection. The result is an exceptionally robust strategy that
can work on a wide range of data sets, including those very close to focus or
with very short integration times, obviating the need for particle polishing.
Application significantly improves Thon ring quality and 3D reconstruction
resolution.

Cite publication: Shawn Q. Zheng, Eugene Palovcak, Jean-Paul Armache, Yifan
Cheng and David A. Agard (2016) Anisotropic Correction of Beam-induced Motion
for Improved Single-particle Electron Cryo-microscopy, Nature Methods,
submitted. BioArxiv: http://biorxiv.org/content/early/2016/07/04/061960
"

