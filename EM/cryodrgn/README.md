# cryoDRGN

CryoDRGN is installed in a central conda environment. 
## Adding New Versions

To add a new version, first create the conda environment, as described in
../../Programming/anaconda/2019.07/conda-env-defs/cryodrgn/README.md.

Next, update files/variants with the new version and run `./build <VERSION>` to
install the modulefile

