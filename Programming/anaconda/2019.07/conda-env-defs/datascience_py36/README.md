Note that the dask 2.1.0 with python 3.6 led to a non-functioning configuration (serialization errors in dask). Therefore we exclude it here.

It would be better if users used the datascience_py37 environment.
