#%Module1.0

module-whatis		"open source MPI-2 implementation"
module-url		"http://www.open-mpi.org/"
module-license		"See \$OPENMPI_DIR/share/doc/openmpi/LICENSE"
module-maintainer	"Achim Gsell <achim.gsell@psi.ch>"
module-help	"
The Open MPI Project is an open source MPI-2 implementation that is
developed and maintained by a consortium of academic, research, and
industry partners. Open MPI is therefore able to combine the expertise,
technologies, and resources from all across the High Performance Computing
community in order to build the best MPI library available. Open MPI offers
advantages for system and software vendors, application developers and
computer science researchers. 
"

module-addgroup	MPI

setenv		MPICC			$PREFIX/bin/mpicc
setenv		MPICXX			$PREFIX/bin/mpicxx
setenv		MPIF77			$PREFIX/bin/mpif77
setenv		MPIF90			$PREFIX/bin/mpif90
setenv		MPIFC			$PREFIX/bin/mpif90
setenv		MPIFORTRAN		$PREFIX/bin/mpif90
setenv		MPIEXEC			$PREFIX/bin/mpiexec
setenv		MPIRUN			$PREFIX/bin/mpirun

set libibverbs  [glob -nocomplain /usr/lib{64,}/libibverbs.so.1*]
set libnuma     [glob -nocomplain /usr/lib{64,}/libnuma.so.1*]
set librdmacm   [glob -nocomplain /usr/lib{64,}/librdmacm.so.1*]
set libucx      [glob -nocomplain /usr/lib{64,}/libuc\[mpst\].so.0*]
set libpmi      [glob -nocomplain /usr/lib{64,}/libpmi.so.0*]
set libpmi2     [glob -nocomplain /usr/lib{64,}/libpmi2.so.0*]
set libcuda     [glob -nocomplain /usr/lib{64,}/libcuda.so.1*]

if { $libibverbs eq "" || $libnuma eq "" || $librdmacm eq "" || $libucx eq "" || $libpmi eq "" || $libpmi2 eq "" } {
    if { [file isdirectory $PREFIX/lib/fallback] } {
        prepend-path LD_LIBRARY_PATH      $PREFIX/lib/fallback
    }
}

if { $libcuda eq "" } {
    if { [file isdirectory $PREFIX/lib/fallback/cuda] } {
        prepend-path LD_LIBRARY_PATH      $PREFIX/lib/fallback/cuda
    }
}
