#!/usr/bin/env modbuild


declare -rx AR=ar

pbuild::configure() {
	declare config_args=()
	if [[ "${OS}" == "Linux" ]]; then
		BLAS_PREFIX="${OPENBLAS_PREFIX}"
		BLAS_INCLUDE_DIRS="${BLAS_PREFIX}/include"
		BLAS_LIBRARY_DIRS="${BLAS_PREFIX}/lib64"
		BLAS_LIBRARY_NAMES="openblas"
		LAPACK_PREFIX="${OPENBLAS_PREFIX}"

		LAPACK_INCLUDE_DIRS="${LAPACK_PREFIX}/include"
		LAPACK_LIBRARY_DIRS="${LAPACK_PREFIX}/lib64"
		LAPACK_LIBRARY_NAMES="openblas"
		config_args+=( "-DBLAS_LIBRARY_DIRS:PATH=${BLAS_LIBRARY_DIRS}" )
		config_args+=( "-DBLAS_INCLUDE_DIRS:PATH=${BLAS_LIBRARY_DIRS}" )
		config_args+=( "-DBLAS_LIBRARY_NAMES:STRING=${BLAS_LIBRARY_NAMES}" )
		config_args+=( "-DLAPACK_LIBRARY_DIRS:PATH=${LAPACK_LIBRARY_DIRS}" )
		config_args+=( "-DLAPACK_INCLUDE_DIRS:PATH=${LAPACK_INCLUDE_DIRS}" )
		config_args+=( "-DLAPACK_LIBRARY_NAMES:STRING=${LAPACK_LIBRARY_NAMES}" )
	fi
	PARMETIS_INCLUDE_DIRS="${PARMETIS_PREFIX}/include"
	PARMETIS_LIBRARIES="${PARMETIS_PREFIX}/lib/libparmetis.a"
	METIS_INCLUDE_DIRS="${PARMETIS_PREFIX}/include"
	METIS_LIBRARIES="${PARMETIS_PREFIX}/lib/libmetis.a"


	if pbuild::version_lt 12.12.0; then
                config_args+=( "-DTPL_ENABLE_DLlib:BOOL=OFF" )
                config_args+=( "-DTPL_ENABLE_QT:BOOL=OFF" )
                config_args+=( "-DTPL_ENABLE_MPI:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_BLAS:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_LAPACK:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_METIS:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_ParMETIS:BOOL=ON" )
                config_args+=( "-DTPL_METIS_INCLUDE_DIRS:PATH=$METIS_INCLUDE_DIRS" )
                config_args+=( "-DTPL_METIS_LIBRARIES:PATH=$METIS_LIBRARIES" )
                config_args+=( "-DTPL_ParMETIS_INCLUDE_DIRS:PATH=$PARMETIS_INCLUDE_DIRS" )
                config_args+=( "-DTPL_ParMETIS_LIBRARIES:PATH=$PARMETIS_LIBRARIES" )
                config_args+=( "-DTrilinos_ENABLE_Amesos:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Amesos2:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_AztecOO:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Belos:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Epetra:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_EpetraExt:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Galeri:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Ifpack:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Isorropia:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_ML:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_MueLu:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_NOX:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Optika:BOOL=OFF" )
                config_args+=( "-DTrilinos_ENABLE_Teuchos:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Tpetra:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_TESTS:BOOL=OFF" )
	elif pbuild::version_ge 12.12.0; then

		config_args+=( "-DGtest_SKIP_INSTALL:BOOL=ON" )
 		config_args+=( "-DMueLu_ENABLE_Kokkos_Refactor:STRING=ON" )
		config_args+=( "-DMueLu_ENABLE_Kokkos_Refactor_Use_By_Default:STRING=ON" )
 		config_args+=( "-DTpetra_ENABLE_DEPRECATED_CODE:BOOL=OFF" )
		config_args+=( "-DTrilinos_ENABLE_EXPLICIT_INSTANTIATION:BOOL=ON" )
 		config_args+=( "-DTpetra_ENABLE_TESTS:BOOL=OFF" )
		config_args+=( "-DTpetra_ENABLE_EXAMPLES:BOOL=OFF" )
		config_args+=( "-DTpetra_INST_INT_INT:BOOL=OFF" )
		config_args+=( "-DTpetra_INST_INT_LONG:BOOL=ON" )
		config_args+=( "-DTpetra_INST_INT_LONG_LONG:BOOL=OFF" )
 
   		config_args+=( "-DKokkos_ENABLE_Serial:BOOL=ON" )
		config_args+=( "-DKokkos_ENABLE_OpenMP:BOOL=OFF" )
		config_args+=( "-DKokkos_ENABLE_Pthread:BOOL=OFF" )
		config_args+=( "-DKokkos_ENABLE_Cuda:BOOL=OFF" )
		config_args+=( "-DKokkos_ENABLE_Cuda_UVM:BOOL=OFF" )
 		config_args+=( "-DKokkos_ENABLE_Cuda_Lambda:BOOL=OFF" )
		config_args+=( "-DTpetra_INST_SERIAL:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_DLlib:BOOL=OFF" )
                config_args+=( "-DTPL_ENABLE_QT:BOOL=OFF" )
                config_args+=( "-DTPL_ENABLE_MPI:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_BLAS:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_LAPACK:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_METIS:BOOL=ON" )
                config_args+=( "-DTPL_ENABLE_ParMETIS:BOOL=ON" )
                config_args+=( "-DTPL_METIS_INCLUDE_DIRS:PATH=$METIS_INCLUDE_DIRS" )
                config_args+=( "-DTPL_METIS_LIBRARIES:PATH=$METIS_LIBRARIES" )
                config_args+=( "-DTPL_ParMETIS_INCLUDE_DIRS:PATH=$PARMETIS_INCLUDE_DIRS" )
                config_args+=( "-DTPL_ParMETIS_LIBRARIES:PATH=$PARMETIS_LIBRARIES" )
		config_args+=( "-DTrilinos_ENABLE_SEACAS:BOOL=OFF" )
    		config_args+=( "-DTrilinos_ENABLE_OpenMP:BOOL=OFF" )
		config_args+=( "-DTrilinos_ENABLE_MueLu:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Amesos:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Amesos2:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_AztecOO:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Belos:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Epetra:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_EpetraExt:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Galeri:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Ifpack:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Isorropia:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_ML:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_MueLu:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_NOX:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Optika:BOOL=OFF" )
                config_args+=( "-DTrilinos_ENABLE_Teuchos:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_Tpetra:BOOL=ON" )
                config_args+=( "-DTrilinos_ENABLE_TESTS:BOOL=OFF" )
	fi

	cmake \
           -D CMAKE_BUILD_TYPE:STRING=Release \
           -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX \
           -DCMAKE_CXX_FLAGS:STRING="-DMPICH_IGNORE_CXX_SEEK -fPIC" \
           -DCMAKE_C_FLAGS:STRING="-DMPICH_IGNORE_CXX_SEEK -fPIC" \
	   -DCMAKE_CXX_STANDARD:STRING="17" \
           -DCMAKE_Fortran_FLAGS:STRING="-fPIC" \
	   "${config_args[@]}" \
           "${SRC_DIR}" || exit 1
}

pbuild::install_docfiles Copyright.txt LICENSE README RELEASE_NOTES

